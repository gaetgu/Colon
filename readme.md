# Colon

*Colon is a SwiftUI for macOS that I have created to aid in the development of a dictionary for a toy spoken language that I am working on.*

Colon is not in any sort of truly usable state, but I want to back up the files so that I do not accidentally those them, and so that I can come back and work on the project when I regain motivation. Feel free to raise issues and make pull requests, but acknowledge that there is no guarantee that I will fix the issues or accept the PRs (though I probably will).
