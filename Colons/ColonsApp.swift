//
//  ColonsApp.swift
//  Colons
//
//  Created by Gabriel Gutierrez on 2/3/22.
//

import SwiftUI

@main
struct ColonsApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }.windowToolbarStyle(UnifiedWindowToolbarStyle(showsTitle: false))
    }
}
