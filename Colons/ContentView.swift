//
//  ContentView.swift
//  Colons
//
//  Created by Gabriel Gutierrez on 2/3/22.
//

import SwiftUI
import CoreData

struct ContentView: View {
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest(sortDescriptors: [NSSortDescriptor(keyPath: \Word.text, ascending: true)], animation: .default) private var words: FetchedResults<Word>
    
    @State var showSearch = false
    @State var showNewWord = false

    var body: some View {
        ZStack {
            NavigationView {
                List {
                    // Verbs Section
                    DisclosureGroup("Verbs") {
                        ForEach(words.filter { ($0.partOfSpeech ?? "unclassified") == "verb" }) { word in
                            WordView(word: word).contextMenu(ContextMenu(menuItems: {
                                Button(action: {
                                    viewContext.delete(word)
                                    
                                    do {
                                        try viewContext.save()
                                    } catch {
                                        let nsError = error as NSError
                                        fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                                    }
                                }, label: {
                                    Text("Delete")
                                })
                            }))
                        }
                    }
                    
                    // Nouns Section
                    DisclosureGroup("Nouns") {
                        ForEach(words.filter { ($0.partOfSpeech ?? "unclassified") == "noun" }) { word in
                            WordView(word: word).contextMenu(ContextMenu(menuItems: {
                                Button(action: {
                                    viewContext.delete(word)
                                    
                                    do {
                                        try viewContext.save()
                                    } catch {
                                        let nsError = error as NSError
                                        fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                                    }
                                }, label: {
                                    Text("Delete")
                                })
                            }))
                        }
                    }
                    
                    // Adjectives Section
                    DisclosureGroup("Adjectives") {
                        ForEach(words.filter { ($0.partOfSpeech ?? "unclassified") == "adjective" }) { word in
                            WordView(word: word).contextMenu(ContextMenu(menuItems: {
                                Button(action: {
                                    viewContext.delete(word)
                                    
                                    do {
                                        try viewContext.save()
                                    } catch {
                                        let nsError = error as NSError
                                        fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                                    }
                                }, label: {
                                    Text("Delete")
                                })
                            }))
                        }
                    }
                    
                    // Unclassified Section
                    DisclosureGroup("Unclassified") {
                        ForEach(words.filter { ($0.partOfSpeech ?? "unclassified") == "unclassified" }) { word in
                            WordView(word: word).contextMenu(ContextMenu(menuItems: {
                                Button(action: {
                                    viewContext.delete(word)
                                    
                                    do {
                                        try viewContext.save()
                                    } catch {
                                        let nsError = error as NSError
                                        fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                                    }
                                }, label: {
                                    Text("Delete")
                                })
                            }))
                        }
                    }
                    
                    // The Dictionary will be here shortly.
                    Spacer()
                    NavigationLink {
                        DictionaryView()
                    } label: {
                        Text("Dictionary")
                    }
                }
                .frame(minWidth: 200)
                Text("Select a word")
            }
            .background(.ultraThinMaterial)
            .toolbar {
                ToolbarItemGroup {
                    if !$showNewWord.wrappedValue {
                        Button(action: toggleSidebar) {
                            Image(systemName: "sidebar.left")
                                .help("Toggle Sidebar")
                        }
                        Spacer()
                        Button(action: {
                            self.showNewWord = true
                        }) {
                            Label("Add Item", systemImage: "plus")
                        }.keyboardShortcut("a", modifiers: [.command, .shift])
                        // Show the search
                        SearchView(isPresented: $showSearch)
                    }
                    if $showNewWord.wrappedValue {
                        Spacer()
                        Button(action: {
                            self.showNewWord = false
                        }) {
                            Label("Cancel", systemImage: "minus")
                        }.keyboardShortcut(.cancelAction)
                    }
                }
            }
            // Show the new word dialog
            if $showNewWord.wrappedValue {
                NewWordView(isPresented: $showNewWord)
            }
        }
    }
    
    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { words[$0] }.forEach(viewContext.delete)
            
            do {
                try viewContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
}

private func toggleSidebar() {
    NSApp.keyWindow?.firstResponder?
        .tryToPerform(#selector(NSSplitViewController.toggleSidebar(_:)), with: nil)
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
