//
//  DictionaryView.swift
//  Colons
//
//  Created by Gabriel Gutierrez on 2/4/22.
//

import SwiftUI

struct DictionaryView: View {
    @FetchRequest(sortDescriptors: [NSSortDescriptor(keyPath: \Word.text, ascending: true)], animation: .default) private var words: FetchedResults<Word>
    
    private var gridItemLayout = [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())]
    
    var body: some View {
//        VStack {
//            ForEach(getWords(), id: \.self) { word in
//                Entry(text: word[0], partOfSpeech: word[1], definition: word[2])
//            }
//        }.padding()
        ScrollView {
            LazyVGrid(columns: gridItemLayout, spacing: 20) {
                ForEach(getWords(), id: \.self) { word in
                    Entry(text: word[0], partOfSpeech: word[1], definition: word[2])
                }
            }
        }.padding(25)
    }
    
    func getWords() -> [[String]] {
        var results: [[String]] = []
        
        for word in words {
            results.append([word.text ?? "<Error loading the text>", word.partOfSpeech ?? "<Error loading part of speeh>", word.definition ?? "<Error loading the definition>"])
        }
        
        return results
    }
}

struct Entry: View {
    @State var text: String
    @State var partOfSpeech: String
    @State var definition: String
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Ellipse()
                    .foregroundColor(Color(NSColor.systemBlue))
                    .frame(width: 6, height: 6)
                
                Text(text)
                    .font(.headline)
                
                Text(partOfSpeech)
                    .font(.subheadline)
                    .foregroundColor(Color(nsColor: .secondaryLabelColor))
            }
            Text(definition).padding(.leading)
        }.frame(maxWidth: .infinity, alignment: .leading)
    }
}
