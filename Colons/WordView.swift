//
//  WordView.swift
//  Colons
//
//  Created by Gabriel Gutierrez on 2/5/22.
//

import SwiftUI

struct WordView: View {
    let word: Word
    
    var body: some View {
        NavigationLink {
            VStack {
                Text(word.text ?? "<ERROR>").font(.system(size: 45)).fontWeight(.bold)
                Text("\"\(word.definition ?? "<ERROR>")\"").font(.system(size: 20)).italic()
                Spacer().frame(height: 100)
                HStack {
                    Text(word.notes ?? "A \(word.partOfSpeech ?? "").").font(.system(size: 15))
                }
            }.frame(maxWidth: .infinity, maxHeight: .infinity).padding(25)
        } label: {
            Text(word.text ?? "<ERROR>")
        }

    }
}
