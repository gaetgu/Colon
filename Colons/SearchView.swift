//
//  SearchView.swift
//  Colons
//
//  Created by Gabriel Gutierrez on 2/5/22.
//

import SwiftUI

struct SearchView: View {
    let wordList = ["Esby", "Mira", "Hadby", "Kro"]
    @Binding var isPresented: Bool
    @State private var searchText = ""
    
    var body: some View {
        NavigationView {
            List {
                ForEach(searchResults(), id: \.self) { word in
                    NavigationLink(destination: Text(word)) {
                        Text(word)
                    }
                }
            }
            .searchable(text: $searchText)
            .navigationTitle("Words")
        }
        Button(action: {
            self.isPresented = false
        }){}.keyboardShortcut(.cancelAction)
    }
    
    func searchResults() -> [String] {
        if $searchText.wrappedValue.isEmpty {
            return wordList
        } else {
            return wordList.filter { $0.contains(searchText) }
        }
    }
}
