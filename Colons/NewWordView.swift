//
//  NewWordView.swift
//  Colons
//
//  Created by Gabriel Gutierrez on 2/3/22.
//

import SwiftUI

struct NewWordView: View {
    @Binding var isPresented: Bool
    
    @State private var type: String = "Choose a type of speech..."
    @State private var text: String = ""
    @State private var definition: String = ""
    @State private var notes: String = ""
    
    @Environment(\.managedObjectContext) private var viewContext
    
    var body: some View {
        VStack {
            Text("New word").font(.largeTitle).frame(maxWidth: .infinity, alignment: .leading)
            
            Menu {
                Button(action: { type = "verb" }) {
                    Text("Verb")
                }
                Button(action: { type = "noun" }) {
                    Text("Noun")
                }
                Button(action: { type = "adjective" }) {
                    Text("Adjective")
                }
                Button(action: { type = "unclassified" }) {
                    Text("Unclassified")
                }
            } label: {
                Text($type.wrappedValue)
            }.frame(width: 200).frame(maxWidth: .infinity, alignment: .leading)
            
            TextField("Text", text: $text).textFieldStyle(RoundedBorderTextFieldStyle()).frame(width: 200).frame(maxWidth: .infinity, alignment: .leading)
            TextField("Definition", text: $definition).textFieldStyle(RoundedBorderTextFieldStyle()).frame(width: 200).frame(maxWidth: .infinity, alignment: .leading)
            
            TextField("Notes", text: $notes).textFieldStyle(RoundedBorderTextFieldStyle()).frame(width: 500).frame(maxWidth: .infinity, alignment: .leading)
            
            if $type.wrappedValue != "Choose a value..." && $text.wrappedValue != "" && $definition.wrappedValue != "" {
                Button(action: addWord) {
                    Text("Done")
                }.keyboardShortcut(.defaultAction)
            }
        }
        .padding().padding().padding()
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
        .background(.ultraThinMaterial)
    }
    
    private func addWord() {
        withAnimation {
            let newWord = Word(context: viewContext)
            newWord.partOfSpeech = $type.wrappedValue
            newWord.text = $text.wrappedValue
            newWord.definition = $definition.wrappedValue
            newWord.notes = $notes.wrappedValue
            
            do {
                try viewContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
        isPresented = false
    }
}
